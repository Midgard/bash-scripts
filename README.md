# Midgard's bash script collection

A collection of some of my useful CLI scripts.

This includes:

* `autobackup`: Automatically create backups of all files in the working directory at given intervals
* `dispf`: Util to make disposable Firefox profiles
* `formatmac`: Read MAC adresses from standard input, write them out to standard output in a standarized way
* `mkdatedir`: Similar to plain `mkdir`, but prepends the date to the directory name

Use the link “Repository” in the site heading to view the files.

